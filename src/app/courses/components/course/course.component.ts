import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import { Course, ICourse } from './../../models/course.model';
import { LoadingService } from '../../../shared';
import { SearchFilterPipe } from '../../pipes';
import { CoursesService } from '../../courses.service';

@Component({
    selector: 'angmp-courses',
    templateUrl: './course.component.html',
    styleUrls: ['./course.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoursesComponent implements OnInit {
    public courses: ICourse[];
    public notFiltredCourses: ICourse[] = [];

    constructor(
        private coursesSvc: CoursesService,
        private loadingSvc: LoadingService,
        private ref: ChangeDetectorRef,
        private searchFilterPipe: SearchFilterPipe
    ) {
        this.courses = [];
    }

    public ngOnInit(): void {
        this.getCourseList();
    }

    public onUpdate(): void {
        this.getCourseList();
    }

    public search(searchString: string): void {
        if (!this.notFiltredCourses || !this.notFiltredCourses.length) {
            this.notFiltredCourses = this.courses.slice();
        }
        this.courses = this.searchFilterPipe.transform(this.notFiltredCourses, searchString);
    }

    private getCourseList(): void {
        this.coursesSvc.getList()
            .delay(2000)
            .subscribe((courses) => {
                this.courses = courses;
                this.loadingSvc.hide();
                this.ref.markForCheck();
            });
    }
}
