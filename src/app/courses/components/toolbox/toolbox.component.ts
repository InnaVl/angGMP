import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Output
} from '@angular/core';

@Component({
    selector: 'angmp-toolbox',
    templateUrl: './toolbox.component.html',
    styleUrls: ['./toolbox.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolboxComponent {
    @Output() public onSearch: EventEmitter<string> = new EventEmitter<string>();

    public search(searchString: string): void {
        this.onSearch.emit(searchString);
    }

}
