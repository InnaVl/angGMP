import {
    Component,
    ChangeDetectionStrategy,
    Output,
    EventEmitter
} from '@angular/core';

@Component({
    selector: 'angmp-search-course',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchCourseComponent {
    @Output() public onSearch: EventEmitter<string> = new EventEmitter<string>();
    public searchValue: string = '';

    public find(): void {
        this.onSearch.emit(this.searchValue);
    }
}
