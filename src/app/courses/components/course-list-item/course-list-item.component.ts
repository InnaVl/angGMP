import { Component, EventEmitter, Input, Output, ChangeDetectionStrategy } from '@angular/core';
import { CoursesService } from '../../courses.service';
import { ICourse } from './../../models/course.model';

@Component({
    selector: 'angmp-course-item',
    templateUrl: './course-list-item.component.html',
    styleUrls: ['./course-list-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CourseItemComponent {
    @Input() public course: ICourse;
    @Output() public onUpdate: EventEmitter<Event> = new EventEmitter<Event>();

    constructor(private coursesSvc: CoursesService) { }

    public delete(): void {
        if (confirm(`Are you sure to delete ${this.course.name}`)) {
            this.coursesSvc.delete(this.course.id);
            this.onUpdate.emit();
        }

    }
}
