import { Component, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'angmp-course-item-controls',
    templateUrl: './course-item-controls.component.html',
    styleUrls: ['./course-item-controls.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CourseItemControlsComponent {
    @Output() public onDelete: EventEmitter<Event> = new EventEmitter<Event>();

    public edit(): void {
        // TODO implemet
    }

    public delete(): void {
        this.onDelete.emit();
    }
}
