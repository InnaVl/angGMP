import { Component, ChangeDetectionStrategy } from '@angular/core';
import { AddCourseService } from '../../add-course.service';
@Component({
    selector: 'angmp-courses-controls',
    templateUrl: './courses-controls.component.html',
    styleUrls: ['./courses-controls.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoursesControlsComponent {
    constructor(private addCourseService: AddCourseService) {
    }

    public add(): void {
        this.addCourseService.isVisible.next(true);
    }
}
