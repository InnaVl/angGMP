import { Component } from '@angular/core';

@Component({
    selector: 'angmp-duration-input',
    templateUrl: './duration-input.component.html',
    styleUrls: ['./duration-input.component.scss']
})
export class DurationInputComponent {
    public duration: number;
}
