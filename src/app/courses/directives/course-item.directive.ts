import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
    selector: '[angmpCourseBorder]'
})
export class CourseBorderDirective implements OnInit {
    @Input() public creationDate: number;

    constructor(private el: ElementRef) { }

    public ngOnInit(): void {
        this.defineBorderStyle();
    }

    private defineBorderStyle(): void {
        const now = new Date().getTime();
        const oneDay = 24 * 60 * 60 * 1000;
        if (this.creationDate) {
            const diffDays = Math.round((now - this.creationDate) / (oneDay));
            if (diffDays < 0) {
                this.el.nativeElement.style.borderColor = '#2E9BBC';
            }
            if (diffDays >= 0 && diffDays <= 14) {
                this.el.nativeElement.style.borderColor = '#ACDBC9';
            }
        }
    }
}
