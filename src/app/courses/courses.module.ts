import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared';
import {
    CourseItemComponent,
    CourseItemControlsComponent,
    CoursesComponent,
    CoursesControlsComponent,
    SearchCourseComponent,
    ToolboxComponent,
    AddCourseComponent,
    DurationInputComponent,
    AuthorPickerComponent
} from './components';
import { CoursesService } from './courses.service';
import { CourseBorderDirective } from './directives';
import {
    DurationPipe,
    OrderByDatePipe,
    SearchFilterPipe
} from './pipes';
import { AddCourseService } from './add-course.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule
    ],
    declarations: [
        CoursesComponent,
        CourseItemComponent,
        CourseItemControlsComponent,
        CoursesControlsComponent,
        SearchCourseComponent,
        ToolboxComponent,
        CourseBorderDirective,
        OrderByDatePipe,
        DurationPipe,
        SearchFilterPipe,
        AddCourseComponent,
        DurationInputComponent,
        AuthorPickerComponent
    ],
    providers: [
        CoursesService,
        SearchFilterPipe,
        AddCourseService
    ],
    exports: [
        CoursesComponent,
        ToolboxComponent,
        AddCourseComponent
    ]
})
export class CoursesModule {
}
