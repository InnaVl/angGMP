export interface IServerCourse {
    id: string;
    name: string;
    description: string;
    date: Date;
    duration: number;
    topRated: boolean;
    other?: string;
}

export interface ICourse {
    id: string;
    name: string;
    description: string;
    date: Date;
    duration: number;
    topRated: boolean;
}

export class Course implements ICourse {
    public id: string;
    public name: string;
    public description: string;
    public date: Date;
    public duration: number;
    public topRated: boolean;

    constructor(course?: ICourse) {
        this.id = (course && course.id) || '';
        this.name = (course && course.name) || '';
        this.description = (course && course.description) || '';
        this.date = (course && course.date) || new Date();
        this.duration = (course && course.duration) || 0;
        this.topRated = (course && course.topRated) || false;
    }
}
