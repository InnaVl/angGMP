import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Injectable()
export class AddCourseService {
    public isVisible: ReplaySubject<boolean> = new ReplaySubject(1);
}
