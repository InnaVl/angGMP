import { Pipe } from '@angular/core';
import { PipeTransform } from '@angular/core/src/change_detection/pipe_transform';
import { ICourse } from '../models/course.model';

@Pipe({
    name: 'angmpOrderByDate'
})
export class OrderByDatePipe implements PipeTransform {
    public transform(courses: ICourse[]): ICourse[] {
        if (courses && courses.length) {
            return courses.sort((a: ICourse, b: ICourse) => {
                if (a.date > b.date) {
                    return -1;
                } else
                    if (a.date === b.date) {
                        return 0;
                    } else {
                        return 1;
                    }
            });
        } else {
            return courses;
        }
    }
}
