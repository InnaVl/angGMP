import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'angmpDuration'
})
export class DurationPipe implements PipeTransform {
    public transform(duration: number): string {
        if (duration) {
            const hours = Math.trunc(duration / 60);
            const minutes = duration % 60;
            return !!hours ?
                !!minutes ?
                    `${hours} h ${minutes} m` :
                    `${hours} h` :
                `${minutes} m`;
        }
        return '';
    }
}
