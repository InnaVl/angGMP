import { Pipe, PipeTransform } from '@angular/core';
import { ICourse } from '../models/course.model';

@Pipe({
    name: 'angmpSearchFilter'
})
export class SearchFilterPipe implements PipeTransform {
    public transform(courses: ICourse[], searchString: string): ICourse[] {
        if (courses && courses.length && searchString) {
            return courses.filter((course) => {
                return course.name.toLocaleLowerCase()
                    .includes(searchString.toLocaleLowerCase());
            });
        } else {
            return courses;
        }
    }
}
