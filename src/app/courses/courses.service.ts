import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { LoadingService } from './../shared/components/loading/loading.service';
import { Course, ICourse, IServerCourse } from './models/course.model';

@Injectable()
export class CoursesService {

    private serverRes: IServerCourse[] = [
        {
            id: 'VID_1',
            name: 'course1',
            duration: 60,
            date: new Date('1/2/2019'),
            description: 'lorem',
            topRated: true,
            other: 'blah-blah'
        },
        {
            id: 'VID_4',
            name: 'course1',
            duration: 30,
            date: new Date('1/2/2011'),
            description: 'lorem',
            topRated: true,
            other: 'blah-blah'
        },
        {
            id: 'VID_2',
            name: 'course2',
            duration: 160,
            date: new Date('1/1/2013'),
            description: 'lorem lorem lorem',
            topRated: true
        },
        {
            id: 'VID_2',
            name: 'course2',
            duration: 160,
            date: new Date('3/20/2018'),
            description: 'lorem lorem lorem',
            topRated: false,
            other: 'blah-blah'
        }
    ];

    constructor(private loadingSvc: LoadingService) { }

    public getList(): Observable<ICourse[]> {
        const now = new Date().getTime();
        const oneDay = 24 * 60 * 60 * 1000;
        this.loadingSvc.show();
        return Observable.of(this.serverRes)
            .map((collection: ICourse[]) => {
                return collection
                    .filter((item) => {
                        return (Math.round(now / oneDay - item.date.getTime() / oneDay)) <= 14;
                    })
                    .map((item: ICourse) => {
                        return {
                            id: item.id,
                            name: item.name,
                            duration: item.duration,
                            date: item.date,
                            description: item.description,
                            topRated: item.topRated
                        };
                    });
            });
    }

    public createCourse(course: ICourse): void {
        this.serverRes.push(new Course(course));
    }

    public getItemByID(id: string): Observable<ICourse> {
        return Observable.of(this.serverRes.find((course) => id === course.id));
    }

    public updateItem(id: string, updatedCourse: ICourse): void {
        const updatedIndex = this.serverRes.findIndex((course) => id === course.id);
        this.serverRes[updatedIndex] = updatedCourse;
    }

    public delete(id: string): void {
        const deletedIndex = this.serverRes.findIndex((course) => id === course.id);
        this.serverRes.splice(deletedIndex, 1);
    }

}
