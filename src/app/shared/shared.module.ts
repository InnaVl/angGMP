import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoadingService } from './components/loading/loading.service';
import {
    CustomButtonComponent,
    LoadingComponent,
    DatePickerComponent
} from './components';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        CustomButtonComponent,
        LoadingComponent,
        DatePickerComponent
    ],
    providers: [
        LoadingService
    ],
    exports: [
        CustomButtonComponent,
        LoadingComponent,
        DatePickerComponent
    ]
})
export class SharedModule {
}
