import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { CustomButtonComponent } from './button.component';
describe('Shared :: ButtonComponent', () => {
    let fixture: ComponentFixture<CustomButtonComponent>;
    let component: CustomButtonComponent;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            declarations: [CustomButtonComponent]
        });
        fixture = TestBed.createComponent(CustomButtonComponent);
        component = fixture.componentInstance;
    });
    it('should be defined', () => {
        expect(component).toBeDefined();
    });

    // it('should emit on click', () => {
    //     spyOn(component.onClick, 'emit');
    //     const nativeElement = fixture.nativeElement;
    //     const button = nativeElement.querySelector('button');
    //     button.dispatchEvent(new Event('click'));
    //     fixture.detectChanges();
    //     expect(component.onClick.emit).toHaveBeenCalled();
    // });
});
