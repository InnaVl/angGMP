import { Component, EventEmitter, Input, Output, ChangeDetectionStrategy } from '@angular/core';
@Component({
    selector: 'angmp-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomButtonComponent {
    @Input() public text: string;
    @Input() public type: string;
    @Output() public onClick: EventEmitter<Event> = new EventEmitter<Event>();

    public onButtonClick(event: Event): void {
        this.onClick.emit(event);
    }

}
