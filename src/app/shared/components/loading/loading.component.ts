import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { LoadingService } from './loading.service';

@Component({
    selector: 'angmp-loading',
    templateUrl: './loading.component.html',
    styleUrls: ['./loading.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoadingComponent {
    @Input() public loadingMsg: string = 'Loading...';

    public isLoading: ReplaySubject<boolean> = this.loadingSvc.isLoading;

    constructor(private loadingSvc: LoadingService) { }
}
