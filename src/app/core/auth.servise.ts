import { Injectable } from '@angular/core';
import { ICredential } from './models/credential.model';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { ReplaySubject } from 'rxjs';

@Injectable()
export class AuthService {
    public isAuthenticated: Subject<boolean> = new Subject<boolean>();
    public userInfo: ReplaySubject<string> = new ReplaySubject(1);

    public login(credentials: ICredential): void {
        localStorage.setItem('username', credentials.login);
        localStorage.setItem('token', this.getGUIDToken());
        this.isAuthenticated.next(true);
        this.getUserInfo();
    }

    public logout(): void {
        localStorage.clear();
        this.isAuthenticated.next(false);
        this.userInfo.next('');
    }

    private getUserInfo(): void {
        this.userInfo.next(localStorage.getItem('username'));
    }

    private getGUIDToken(): string {
        return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
    }
    private s4(): string {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
}
