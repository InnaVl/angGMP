import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { LoadingService } from './../../../shared';
import { AuthService } from '../../auth.servise';
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Component({
    selector: 'angmp-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent {
    public login: string = '';
    public pass: string = '';

    constructor(
        private authSvc: AuthService,
        private ref: ChangeDetectorRef,
        private loadingSvc: LoadingService) { }

    public signIn(): void {
        this.loadingSvc.show();
        this.authSvc.login({ login: this.login, pass: this.pass });
        this.ref.markForCheck();
    }
}
