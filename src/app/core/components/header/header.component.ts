import { Observable } from 'rxjs/Observable';
import { Component, ChangeDetectionStrategy, OnInit, ChangeDetectorRef, NgZone } from '@angular/core';
import { AuthService } from './../../auth.servise';

@Component({
    selector: 'angmp-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {
    public isAuth: boolean;
    public userInfo: Observable<string>;
    private timeStart: number;

    constructor(
        private authSvc: AuthService,
        private ref: ChangeDetectorRef,
        private ngZone: NgZone
    ) {
    }

    public ngOnInit(): void {
        this.subscribeForAuth();
        this.subscribeForUserInfo();
    }

    public logOut(): void {
        this.authSvc.logout();
    }

    private subscribeForAuth(): void {
        this.authSvc.isAuthenticated
            .subscribe((flag: boolean) => {
                this.isAuth = flag;
                this.ref.markForCheck();
            });
    }
    private subscribeForUserInfo(): void {
        this.userInfo = this.authSvc.userInfo;
    }
}
