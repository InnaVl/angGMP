import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'angmp-breadcrumbs',
    templateUrl: './breadcrumbs.component.html',
    styleUrls: ['./breadcrumbs.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BreadCrumbsComponent {

}
