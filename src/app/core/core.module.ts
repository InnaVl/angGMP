import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './../shared/shared.module';
import { AuthService } from './auth.servise';
import {
    BreadCrumbsComponent,
    FooterComponent,
    HeaderComponent,
    LoginComponent
} from './components';

@NgModule({
    imports: [
        FormsModule,
        SharedModule,
        CommonModule
    ],
    declarations: [
        BreadCrumbsComponent,
        FooterComponent,
        HeaderComponent,
        LoginComponent
    ],
    providers: [
        AuthService
    ],
    exports: [
        FooterComponent,
        HeaderComponent,
        LoginComponent
    ]
})
export class CoreModule {
}
