export interface ICredential {
    login: string;
    pass: string;
}
