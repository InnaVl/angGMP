import { Component, ChangeDetectionStrategy, ChangeDetectorRef, OnInit } from '@angular/core';
import { AuthService } from './core/auth.servise';
import { LoadingService } from './shared/components/loading/loading.service';
import { AddCourseService } from './courses/add-course.service';

@Component({
    selector: 'angmp-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
    public isAuth: boolean;
    public isAddCourseVisible = this.addCourseService.isVisible;

    constructor(
        private authService: AuthService,
        private addCourseService: AddCourseService,
        private ref: ChangeDetectorRef) {
    }

    public ngOnInit(): void {
        this.subscribeToAuth();
    }

    public subscribeToAuth(): void {
        this.authService.isAuthenticated.subscribe((flag) => {
            this.isAuth = flag;
        });
    }
}
