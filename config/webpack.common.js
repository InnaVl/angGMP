var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var helpers = require('./helpers');

module.exports = {
    entry: {
        'polyfills': './src/polyfills.ts',
        'vendor': './src/vendor.ts',
        'app': './src/main.ts'
    },

    resolve: {
        extensions: ['.ts', '.js']
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                loaders: [
                    {
                        loader: 'awesome-typescript-loader',

                        options: {
                            configFileName: helpers.root('src', 'tsconfig.json')
                        }
                    }, 'angular2-template-loader'
                ],
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                loader: 'file-loader?name=assets/[name].[hash].[ext]'
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: ['to-string-loader', 'style-loader', 'css-loader', 'sass-loader']
            },
        ]
    },

    plugins: [
        new webpack.ContextReplacementPlugin(new RegExp(/\@angular(\\|\/)core(\\|\/)(\@angular|esm5)/),
            helpers.root('./src'),
            {}
        ),

        new webpack.optimize.CommonsChunkPlugin({
            name: ['app', 'vendor', 'polyfills']
        }),
        new HtmlWebpackPlugin ({
            inject: true,
            template: 'src/index.html'
          })
    ]
};
