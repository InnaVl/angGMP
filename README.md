# *Angular mp tasks*

    

## *Task 1: Intro*

    Set up build(dev, prod);
    Setup TS lint according to your preferences (indentation, spacing...);
    Create project structure including all components with blank content;
    Create header, footer fake components;
    Create courses, course details, login page fake components
    Create course typescript interface based on mock up. All fields are presented at the page

## *Task 2: Components*

    Create page with the whole courses list. 
    Use mocked array of courses implemented as property of courses page components.
    Use Interface of course item created previously.
    Create item class that implements your interface.
    Implement markup + styles + simple logic for components below.

    1. Logo block
        Insert any image and name for right side of the component
        Courses - it’s breadcrumbs in future. Skip it for now.
        Leave user login as is (text placeholder)
        Log off - fake placeholder
        Overall - just empty component with some markup and css
    2.Toolbox component

        Place input field, button find and add course button.
        Bind [()] input field with component property.
        On Find click - console.log input value
        Add course button - non-functional
    3. Course item
        Implement @Input for course-item
        Use {{object.property}} construction to show all related information (title, duration, date, description)
        Edit course - not implemented, markup only
        Delete - use @Output to call method on parent component
        You should console.log course id in video courses page component
    4. Footer component
        Simple markup + css
        You can use courses page component directly without using routing.
        Place <courses-page></courses-page> into app component.

    Learn some lifecycle hooks during implementing component.
    Try to declare courses without  array then assign empty array in constructor and after that assign fake values on ngOnInit hook.
    Place console.log statement in hooks to understand ordering.
    Use ngFor to iterate through your fake items and pass it to the course item component.
    
## *Task 3: Modules. Services*

    Create service for courses. Implement next methods:
        - Get list
        - Create course
        - Get item by id
        - Update item
        - Remove item
    Do not use http module to solve a task.
    Implement static collection of courses in the service.
    Remove static array in your courses component. Use service instead. Call service for items in appropriate lifecycle hook.
    Implement delete action.
    Add confirmation modal on delete item. (Do you really want to delete this course? Yes/No). 
    Create authorization service. Implement next methods:
        - Login (stores fake user info and token to local storage)
        - Logout (wipes fake user info and token from local storage)
        - IsAuthenticated (boolean)
        - GetUserInfo (returns user login)
    Use authService in header component. Implement fake action on logout (wipe user info, console.log etc...)
    Implement Login page:

    Reuse header/footer components. Implement logic to hide breadcrumbs and user info block (call isAuth and act corresponding to result. Use *ngIf. Yes, we did not pass directives, but use it for now. Simple directive to show/hide content)
    Create markup, module and component for login page. Do not implement any validation here. No form for this step. Just bind value to component class as you done in previous task.
    On Enter button click - use login method of your authService. Then just console log or something else. Fake for now.
    You don’t have to make routing. At this step we don’t know what routing is. So please use familiar construction below:
    Place <login-page></login-page> into app component.

## *Task 4: Change Detection*

Add OnPush strategy to ALL components.
Add remove item from list.
Remove course should change fake collection in service.

Add profiling to know how quickly change detection tree gets stable.
You should compare default strategy vs OnPush by spent time. To perform this comparison please do follow:
Subscribe to this.ngZone.onUnstable and this.ngZone.onStable events.
Therefore you will be able to know two time points: start (unstable) and end (stable). Given the difference between these time points you will know how quickly detection tree gets stable.
(Profiling example from the lecture uses deprecated API, so demo example will result in error.
Please follow the latest reference https://angular.io/docs/ts/latest/api/core/index/NgZone-class.html)
(Angular is already hooked by zone. You should simply find corresponding method in ngZone. Please follow the link from previous answer.)
Extra task based on Observables (optional for 5):
Change return type of services to Observable.
Make userInfo as a public property of auth service of Observable type.
Subscribe in header on change of this public property. So you will able to change username (hide) on logout action. And display on login.
Since we changed strategy to OnPush - don’t forget to use mark for check method.
Create loaderBlock component with loaderBlock service.
loaderBlock should be blocking overlay with spinner (or any other design) Example: https://projects.lukehaas.me/css-loaders/
With transparent overlay: https://gyazo.com/da0d84b528d4a5a2174930ab09960562
Have boolean property show.
Subscribed to related property of loaderBlock service and listen for changes.


loaderBlock service has two methods:
Show
Hide
Use created loaderBlock on courses page. Call it on delete action.
Use loaderBlock on login page to prevent user actions (typing username/password, clicking login button again).


## *Task 5: Change Detection*
    Directives:
Create custom directive to change course plate border based on createdDate.
If createdDate < currentDate && createdDate >= currentDate - 14days - fresh course (use green border)
If createdDate > currentDate - upcoming course (use blue border)
Otherwise - no changes
Add no data message using ngIf.

Add topRated field to course. If true - show star icon, change background color using ngClass.


Pipes: 
Use built-in pipe to uppercase video course name.
Create duration pipe. Should take duration in minutes and format output in 
hh h mm min. (ex: 1h 15min). In case of duration less than 1h display only minutes.
Use built-in date pipe for course date. Change mock data to have datetime createdDate field instead of preformatted string.

Create orderBy pipe. Use createdDate field to order courses.
Change find behavior
 
Courses list is filtered by name when user enters some text in the field ‘search’ and presses the ‘Find’ button. (we need this task in order to try using pipes in component class not in the template)Criteria for evaluation


## *Task 6: RxJs*

Courses page enhancements
Change all service responses to Observables. (use Observable.of(...))
You should subscribe on observables in component.
Don’t forget to unsubscribe.
Filter out outdated courses (date < currentDate - 14days).
Use map function to cast response shape to data model. (since BE side is not implemented yet - pretend to have fake collection not to match data model).
Use replay subject in auth service to reveal user info.
Create public property of ReplaySubject. Call .next on login and logout methods.
Use replay subject in loading block.
Pass new value on show/hide call.
Update ‘createdDate’ field in model to ‘date’. (naming issue from previous tasks).

Course add/edit page markup + basic structure

Create add course page. (don’t need to create routing. Do the same as for login page)
Create markup

    1. No changes
    2. Text input
    3. Text area
    4. Separate component. Text input for now.
    5. Separate component. Input in minutes. Eval duration aside of input with pipe created before.
    6. Separate component. Skip it for now. Will be different.
    7. Buttons block. Add empty handlers.